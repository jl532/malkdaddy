# Simple demo of reading each analog input from the ADS1115 from the PPG sensor
# Author: Jason Liu for BME462 with Dr. Malkin
# License: Public Domain


## import commands -------------------------------------------------

# import time library
import time

# Import the ADS1x15 module.
import Adafruit_ADS1x15

# importing CSV for data analysis of reading
import csv

# import pylot in general
import matplotlib.pyplot as plt

# import animations for pyplot
import matplotlib.animation as animation
#matplotlib.use('Agg')

# Create an ADS1115 ADC (16-bit) instance.
adc = Adafruit_ADS1x15.ADS1115()

## /import commands--------------------------------------------------

# Note you can change the I2C address-- in case you want to add more I2C-enabled things from its default (0x48), and/or the I2C
# bus by passing in these optional parameters:
#adc = Adafruit_ADS1x15.ADS1015(address=0x49, busnum=1)


# The following is specifically for changing the gain on the ADC. Not changed (1) for the PPG sensor
# Choose a gain of 1 for reading voltages from 0 to 4.09V-- for the ADC
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
GAIN = 1

# Main loop. # each loop needs to run faster than .375 seconds per loop to catch all relevant beatdata
# for regular humans this value can be 0.75.
print("initializing...")

start_time=time.time()

five_iterator_bool=True
step_detected=True
beat_delaytimer=-0.5
five_index=0
five_data_list = []
beat_detected = []
instant_rate=60
sample_length = 15

five_data_list.append([])
five_data_list.append([])
five_data_list.append([])

for mainiter in range(0,5):
    print("new five loop initiating" + str(mainiter) + "@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#");
    plt.clf()
    while five_iterator_bool:
            # scales the PPG value to between 0 and 5 (originally from 0-26123)
            ppg_currentvalue_corrected5=5.0*(float(adc.read_adc(3,gain=GAIN)) /26123.0)
            five_loop_timeelapsed=time.time()-start_time
            five_data_list[0].append(ppg_currentvalue_corrected5)
            five_data_list[1].append(five_loop_timeelapsed)
            #print("iteration time:" + str(five_loop_timeelapsed));
            time.sleep(0.03)
            if five_index>4:
                    past_several_avg=sum(five_data_list[0][(five_index-5):five_index])/5
                    if past_several_avg > 3 and time.time()-beat_delaytimer>0.5:
                            beat_detected.append(True)
                            if sum(beat_detected) > 1:
                                    instant_period=time.time()-beat_delaytimer
                                    instant_rate=60/instant_period
                                    five_data_list[2].append(instant_rate)
                                    print("Heart Rate: " + str(instant_rate))
                            print("beat detected-------------------------------------")
                            beat_delaytimer=time.time()
                    else:
                            beat_detected.append(False)
            if len(five_data_list[0]) > len(five_data_list[2]):
                heart_rate_list = five_data_list[2]
                heart_rate = -1 if len(heart_rate_list) == 0 else heart_rate_list[len(heart_rate_list) - 1]
                five_data_list[2].append(heart_rate)
            five_index+=1
            if five_loop_timeelapsed > sample_length:
                    five_iterator_bool=False
    print("five_loop successful! beats detected:")
    print(sum(beat_detected))

    fig=plt.figure()
    ax=plt.axes(xlim=(0,5),ylim=(0,5))
    line, = ax.plot([], [] , lw=2)

    plt.xlabel("Time (seconds)")
    plt.ylabel("Voltage (volts)")
    title = "Pulse Plethysmograph output window"
    plt.title(title)
    hr_box = ax.text(2.5, 2.5, '')

    def init():
            line.set_data([], [])
            return line,

    def animate(i):
            x=five_data_list[1]
            y=five_data_list[0]
            heart_rate_list = five_data_list[2]
            line.set_data(x[:i],y[:i])
            if x[i] > 4:
                ax.set_xlim(x[i] - 4, x[i] + 1)
            else:
                ax.set_xlim(0, 5)

            try:
                if heart_rate_list[i] >= 0:
                    hr_text = '%.1f' % heart_rate_list[i] + " BPM"
                    hr_box.set_text(hr_text)
                    hr_box.set_x(x[i])
                    hr_box.set_y(y[i])
            except (IndexError):
                    hr_box.set_text('')
            return line,
    frames = len(five_data_list[0])
    interval = float(sample_length) / len(five_data_list[0]) * 1000.0
    print(interval)
    anim = animation.FuncAnimation(fig, animate, init_func=init, frames=frames, interval=interval, blit=False)
    plt.show()

#anim.save('first_success_5sec_JL.mp4',fps=30) video output doesn't quite work

print("plot successful")


print("program terminated successfully")


      
           
        
